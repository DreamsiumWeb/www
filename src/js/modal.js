/* global define */
define(['addDarkener', 'removeDarkener'],
  function (addDarkener, removeDarkener) {
    return function modal (modalName, modalText) {
      return new Promise(function (resolve, reject) {
        addDarkener()

        // create div
        var modalDiv = document.createElement('div')
        modalDiv.classList.add('plasticModal')

        var modalTitleDiv = document.createElement('div') // create title container
        modalTitleDiv.classList.add('plasticModalTitleContainer')
        var modalDomTitle = document.createElement('h3') // create the h3
        var node = document.createTextNode(modalName)
        modalDomTitle.appendChild(node) // append h3
        modalTitleDiv.appendChild(modalDomTitle)

        var modalTextDiv = document.createElement('div')
        modalTextDiv.classList.add('plasticModalTextContainer')
        var modalDomText = document.createElement('p')
        var node2 = document.createTextNode(modalText)
        modalDomText.appendChild(node2)
        modalTextDiv.appendChild(modalDomText)

        // create div
        var modalButtonDiv = document.createElement('div')
        modalButtonDiv.classList.add('plasticModalButtonContainer')

        // add continue button
        var continueButton = document.createElement('button')
        var continueIndicator = document.createElement('i')
        continueIndicator.classList.add('dream-rightarrow')
        continueButton.appendChild(continueIndicator)
        modalButtonDiv.appendChild(continueButton)

        // add close button
        var closeButton = document.createElement('button')
        var closeIndicator = document.createElement('i')
        closeIndicator.classList.add('dream-close')
        closeButton.classList.add('plasticModalButtonAlternative')
        closeButton.appendChild(closeIndicator)
        modalButtonDiv.appendChild(closeButton)

        // give it an ID
        require(['genUUID'], function (genUUID) {
          modalDiv.setAttribute('id', genUUID())
        })

        // attach to body
        modalDiv.appendChild(modalTitleDiv)
        modalDiv.appendChild(modalTextDiv)
        modalDiv.appendChild(modalButtonDiv)
        document.getElementsByTagName('body')[0].appendChild(modalDiv)

        // function to animate the close
        function animateClose () {
          modalDiv.classList.add('plasticModalTempRemove')
          removeDarkener()
          setTimeout(
            function () {
              modalDiv.parentNode.removeChild(modalDiv)
            }, 200
          )
        }

        continueButton.addEventListener('click', function () {
          animateClose()
          resolve(true)
        })

        closeButton.addEventListener('click', function () {
          animateClose()
          resolve(false)
        })
      })
    }
  }
)
