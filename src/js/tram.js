require(['modal', 'toast'], function (modal, toast) {
  // Code link
  document.getElementsByTagName('a')[0].addEventListener('click', function () {
    modal('Leaving Dreamsium', "The website you're about to visit isn't controlled by Dreamsium. Would you like to continue?").then(function (response) {
      console.log(response)
      if (response === true) {
        toast('Redirecting..')
        window.location.href = 'https://gitlab.com/Dreamsium/Tram'
      }
    }, function (error) {
      console.error(error)
    })
  })
})
