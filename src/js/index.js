require(['modal', 'toast'], function (modal, toast) {
  // Tram
  document.getElementsByClassName('card')[0].addEventListener('click', function () {
    toast('Redirecting..')
    window.location.href = 'tram.html'
  })

  // Courier & MIST
  document.getElementsByClassName('card')[1].addEventListener('click', function () {
    toast('Redirecting..')
    window.location.href = 'courier.html'
  })

  // Intersect
  document.getElementsByClassName('card')[2].addEventListener('click', function () {
    toast('Coming soon.')
  })

  // Plastic
  document.getElementsByClassName('card')[3].addEventListener('click', function () {
    modal('Coming soon.', ';)').then(function (response) {
      console.log(response)
      if (response === true) toast("You're using it right now.")
    }, function (error) {
      console.error(error)
    })
  })

  // Team
  document.getElementsByClassName('card')[4].addEventListener('click', function () {
    toast('Coming soon.')
  })

  // Devel
  document.getElementsByClassName('card')[5].addEventListener('click', function () {
    toast('Coming soon.')
  })

  // Go
  document.getElementsByClassName('card')[6].addEventListener('click', function () {
    toast('Coming soon.')
  })

  // Libre
  document.getElementsByClassName('card')[7].addEventListener('click', function () {
    toast('Coming soon.')
  })

  document.getElementsByTagName('a')[0].addEventListener('click', function () {
    modal('Leaving Dreamsium', "The website you're about to visit isn't controlled by Dreamsium. Would you like to continue?").then(function (response) {
      console.log(response)
      if (response === true) {
        toast('Redirecting..')
        window.location.href = 'https://twitter.com/Dreamsium'
      }
    }, function (error) {
      console.error(error)
    })
  })

  document.getElementsByTagName('a')[1].addEventListener('click', function () {
    toast('Redirecting..')
  })

  document.getElementsByTagName('a')[2].addEventListener('click', function () {
    modal('Leaving Dreamsium', "The website you're about to visit isn't controlled by Dreamsium. Would you like to continue?").then(function (response) {
      console.log(response)
      if (response === true) {
        toast('Redirecting..')
        window.location.href = 'https://discord.gg/FFVz3k6'
      }
    }, function (error) {
      console.error(error)
    })
  })

  document.getElementsByTagName('a')[3].addEventListener('click', function () {
    modal('Leaving Dreamsium', "The website you're about to visit isn't controlled by Dreamsium. Would you like to continue?").then(function (response) {
      console.log(response)
      if (response === true) {
        toast('Redirecting..')
        window.location.href = 'https://gitlab.com/Dreamsium'
      }
    }, function (error) {
      console.error(error)
    })
  })
})
