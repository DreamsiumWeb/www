/* global define */
define(
  function () {
    return function removeDarkener () {
      document.getElementsByClassName('plasticDarkener')[0].classList.add('plasticDarkenerTempRemove')
      setTimeout(function () {
        document.getElementsByClassName('plasticDarkener')[0].parentNode.removeChild(document.getElementsByClassName('plasticDarkener')[0])
      }, 240)
    }
  }
)
