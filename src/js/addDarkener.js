/* global define */
define(
  function () {
    return function addDarkener () {
      // create div
      var darkenerDiv = document.createElement('div')
      darkenerDiv.classList.add('plasticDarkener')
      // attach to body
      document.getElementsByTagName('body')[0].appendChild(darkenerDiv)
    }
  }
)
