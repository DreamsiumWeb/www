/* global define */
define(
  function () {
    return function toast (toastName) {
      // create div
      var toastDiv = document.createElement('div')
      toastDiv.classList.add('plasticToast')

      // add text
      var domTitle = document.createElement('h3')
      var node = document.createTextNode(toastName)
      domTitle.appendChild(node)
      toastDiv.appendChild(domTitle)

      // add close button
      var button = document.createElement('button')
      var closeIndicator = document.createElement('i')
      closeIndicator.classList.add('dream-close')
      button.appendChild(closeIndicator)
      toastDiv.appendChild(button)

      // give it an ID
      require(['genUUID'], function (genUUID) {
        toastDiv.setAttribute('id', genUUID())
      })

      // attach to body
      document.getElementsByTagName('body')[0].appendChild(toastDiv)

      // function to animate the close
      function animateClose () {
        toastDiv.classList.add('plasticToastTempRemove')
        setTimeout(
          function () {
            toastDiv.parentNode.removeChild(toastDiv)
          }, 250
        )
      }

      // delete on button click
      button.addEventListener('click', function () {
        animateClose()
      })

      // delete automatically
      setTimeout(
        function () {
          if (toastDiv.parentNode !== null) {
            animateClose()
          }
        }, 4000
      )
    }
  }
)
