/* eslint-disable */
({
  appDir: './src',
  baseUrl: './js',
  dir: './out',
  modules: [
      {
          name: 'index',
          name: 'modal',
          name: 'toast',
          name: 'genUUID',
          named: 'removeDarkener'
      }
  ],
  optimizeCss: 'standard',
  removeCombined: true
})